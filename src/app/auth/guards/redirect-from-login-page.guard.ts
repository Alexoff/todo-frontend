import {CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({ providedIn: 'root' })
export class RedirectFromLoginPageGuard implements CanActivate {
  constructor(private router: Router, private jwtService: JwtHelperService) { }

  canActivate(): boolean {
    const token: string = localStorage.getItem('x-access-token');
    if (token && !this.jwtService.isTokenExpired(token)) {
      this.router.navigate(['/todo']);
      return false;
    }
    localStorage.removeItem('x-access-token');
    return true;
  }
}
