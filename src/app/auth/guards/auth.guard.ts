import { CanActivate, Router, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private jwtService: JwtHelperService) { }

  canActivate(): boolean | UrlTree {
    const token: string = localStorage.getItem('x-access-token');
    if (token && !this.jwtService.isTokenExpired(token)) {
      return true;
    }
    localStorage.removeItem('x-access-token');
    this.router.navigate(['/login']);
    return false;
  }
}
