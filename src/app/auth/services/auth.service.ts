import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {AuthModel} from '../models/auth.model';
import {tap} from 'rxjs/operators';
import {AuthResultModel} from '../models/auth-result.model';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public baseUrl = environment.baseUrl;

  constructor(private http: HttpClient,
              private router: Router) {
  }

  public login(login: AuthModel) {
    return this.http.post<AuthResultModel>(`${this.baseUrl}/auth/login`, login).pipe(
      tap(tokens => {
        localStorage.setItem('x-access-token', tokens.accessToken);
        this.router.navigate(['/todo']);
      })
    );
  }
}
