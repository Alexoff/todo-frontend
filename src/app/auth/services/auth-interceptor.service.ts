// import {Injectable} from '@angular/core';
// import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
// import {AuthService} from './auth.service';
// import {exhaustMap, take} from 'rxjs/operators';
//
// @Injectable({
//   providedIn: 'root'
// })
// export class AuthInterceptorService implements HttpInterceptor {
//
//   constructor(private authService: AuthService) {
//   }
//
//   intercept(req: HttpRequest<any>, next: HttpHandler) {
//         if (!localStorage.getItem('x-access-token')) {
//           return next.handle(req);
//         }
//         const modifiedReq = req.clone({
//           setHeaders: {
//             'x-access-token': localStorage.getItem('x-access-token')
//           }
//         });
//
//         return next.handle(modifiedReq);
//   }
// }
