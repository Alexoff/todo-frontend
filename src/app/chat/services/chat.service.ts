import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {webSocket} from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private url = 'http://localhost:3000';
  private socket;

  constructor() {
  }

  createConnection() {

    this.socket = io(this.url, {
      transports: ['websocket'],
      query: 'x-access-token=' + localStorage.getItem('x-access-token')
    });

    this.socket.on('connection', (socket) => {
      console.log('Connect', socket);
    });
    this.socket.on('message', (event) => console.log(event));
  }

  sendMessage() {
    this.socket.emit('message', {email: 'alex@test.pl', message: 'Hello bro'});
  }

}
