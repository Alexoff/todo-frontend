import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnInit {
  @Input() control: FormGroup | FormControl;

  constructor() {
  }

  ngOnInit(): void {
  }

  get errorMessage() {
    for (const kay in this.control.errors) {
      if (this.control.errors.hasOwnProperty(kay) && this.control.invalid && this.control.touched) {
        return this.getValidationMessage(kay, this.control.errors[kay]);
      }
    }
    return null;
  }

  private getValidationMessage(validatior: string, validatorValue?: any) {
    const messages = {
      required: 'This field required',
      email: 'Wrong email'
    };

    return messages[validatior];
  }
}
