import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from './auth/guards/auth.guard';
import {RedirectFromLoginPageGuard} from './auth/guards/redirect-from-login-page.guard';


const routes: Routes = [
  {path: '', redirectTo: 'todo', pathMatch: 'full'},
  {path: 'login', loadChildren: () => import('./auth/auth.module').then(mod => mod.AuthModule), canActivate: [RedirectFromLoginPageGuard]},
  {path: 'todo', loadChildren: () => import('./todo/todo.module').then(mod => mod.TodoModule), canActivate: [AuthGuard]},
  {path: 'chat', loadChildren: () => import('./chat/chat.module').then(mod => mod.ChatModule), canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
