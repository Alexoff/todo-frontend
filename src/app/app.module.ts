import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        headerName: 'x-access-token',
        authScheme: '',
        tokenGetter: () => {
          return localStorage.getItem('x-access-token');
        },
        whitelistedDomains: ['localhost:3000']
      }
    })
  ],
  providers: [
    JwtHelperService,
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
