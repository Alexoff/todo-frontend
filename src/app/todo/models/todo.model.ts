export class TodoModel {
  public id?: string;
  public name: string;
  public description: string;
  public selected: boolean;
  public markers: Color[];

  constructor(obj) {
    this.id = obj._id;
    this.name = obj.name;
    this.description = obj.description;
    this.selected = obj.selected;
    this.markers = obj.markers.map(marker => new Color(marker));
  }
}


export class Color {
  public id: string;
  public color: string;

  constructor(obj) {
    this.id = obj._id;
    this.color = obj.color;
  }
}
