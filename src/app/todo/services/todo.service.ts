import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {TodoModel} from '../models/todo.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  public baseUrl = environment.baseUrl;

  constructor(public http: HttpClient) {
  }

  createTask(value: TodoModel): Observable<TodoModel[]> {
    return this.http.post<TodoModel[]>(`${this.baseUrl}/todo`, value).pipe(
      map(todos => todos.map(todo => new TodoModel(todo)))
    );
  }

  getAllTodo(): Observable<TodoModel[]> {
    return this.http.get<TodoModel[]>(`${this.baseUrl}/todo`).pipe(
      map(todos => todos.map(todo => new TodoModel(todo)))
    );
  }

  findByName(name: string): Observable<TodoModel[]> {
    return this.http.get<TodoModel[]>(`${this.baseUrl}/todo/${name}`).pipe(
      map(todos => todos.map(todo => new TodoModel(todo)))
    );
  }

  selectedTodo(id: string): Observable<TodoModel[]> {
    return this.http.put<TodoModel[]>(`${this.baseUrl}/todo/selected/${id}`, null).pipe(
      map(todos => todos.map(todo => new TodoModel(todo)))
    );
  }

  deleteTask(id): Observable<TodoModel[]> {
    return this.http.delete<TodoModel[]>(`${this.baseUrl}/todo/${id}`).pipe(
      map(todos => todos.map(todo => new TodoModel(todo)))
    );
  }
}
