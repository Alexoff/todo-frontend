import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {TodoModel} from '../models/todo.model';
import {MarkersModel} from '../models/markers.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {
  public baseUrl = environment.baseUrl;

  constructor(public http: HttpClient) {
  }

  addColor(color: MarkersModel): Observable<MarkersModel[]> {
    return this.http.post<MarkersModel[]>(`${this.baseUrl}/marker`, color).pipe(
      map(markers => markers.map(marker => new MarkersModel(marker)))
    );
  }

  getColors(): Observable<MarkersModel[]> {
    return this.http.get<MarkersModel[]>(`${this.baseUrl}/marker`).pipe(
      map(markers => markers.map(marker => new MarkersModel(marker)))
    );
  }

  selectColor(id: string, color: string) {
    return this.http.put(`${this.baseUrl}/todo/${id}`, {color});
  }
}
