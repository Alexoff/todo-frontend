import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TodoModel} from '../../models/todo.model';
import {TodoService} from '../../services/todo.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() todo: TodoModel;
  @Output() id = new EventEmitter();

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
  }

  onSelected(id: string) {
    this.id.emit(id);
  }

}
