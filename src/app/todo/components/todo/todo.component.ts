import {Component, OnInit} from '@angular/core';
import {TodoService} from '../../services/todo.service';
import {TodoModel} from '../../models/todo.model';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MarkersModel} from '../../models/markers.model';
import {MarkerService} from '../../services/marker.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  public todos: TodoModel[];
  public taskForm: FormGroup;
  public markers: MarkersModel[];

  constructor(public todoService: TodoService, public markerService: MarkerService, private router: Router) {
  }

  ngOnInit(): void {
    this.todoService.getAllTodo().subscribe((todos: TodoModel[]) => {
      this.todos = todos;
      this.prepareForm();
    });
    this.markerService.getColors().subscribe((markers: MarkersModel[]) => this.markers = markers);
  }

  prepareForm() {
    this.taskForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      selected: new FormControl(false, [Validators.required]),
      markers: new FormControl([])
    });
  }

  onSubmit() {
    if (this.taskForm.invalid) {
      return;
    }
    const task = new TodoModel(this.taskForm.value);
    this.todoService.createTask(task).subscribe((todos: TodoModel[]) => this.todos = todos);
  }

  findByName(name: string) {
    this.todoService.findByName(name).subscribe((todos: TodoModel[]) => this.todos = todos);
  }

  deleteTask(id) {
    this.todoService.deleteTask(id).subscribe((todos: TodoModel[]) => this.todos = todos);
  }

  emitId(id: string) {
    this.todoService.selectedTodo(id).subscribe((todos: TodoModel[]) => this.todos = todos);
  }

  logout() {
    // this.authService.logout();
    localStorage.removeItem('x-access-token');
    this.router.navigate(['/login']);
  }
}
