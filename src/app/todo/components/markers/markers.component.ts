import {Component, Input, OnInit} from '@angular/core';
import {MarkersModel} from '../../models/markers.model';
import {MarkerService} from '../../services/marker.service';
import {Color, TodoModel} from '../../models/todo.model';
import {inspect} from 'util';
import {mark} from '@angular/compiler-cli/src/ngtsc/perf/src/clock';

@Component({
  selector: 'app-markers',
  templateUrl: './markers.component.html',
  styleUrls: ['./markers.component.scss']
})
export class MarkersComponent implements OnInit {
  @Input() markerColors;
  @Input() todoId;
  @Input() markers: MarkersModel[];
  public isOpen = false;
  public isAdd = false;
  public color: string;

  constructor(public markerService: MarkerService) {
  }

  ngOnInit(): void {

  }

  saveColor(name: string, colorHex: string) {
    const color = colorHex.replace(/#/gi, '');
    const marker = new MarkersModel({name, color});
    this.markerService.addColor(marker).subscribe((markers: MarkersModel[]) => this.markers = markers);
  }

  selectColor(id: string, color: string) {
    this.markerService.selectColor(id, color).subscribe(() => this.prepareColor(color));
  }

  prepareColor(color: string) {
    const pallete = new Color({_id: 'new id', color});
    let isSplice = false;
    this.markerColors.forEach((marker, index) => {
      if (color === marker.color) {
        this.markerColors.splice(index, 1);
        isSplice = true;
      }
    });
    if (isSplice) {
      return;
    }
    this.markerColors.push(pallete);
  }

  prepareSelectedColor(color: string) {
    let isSelected = false;
    this.markerColors.forEach(marker => {
      if (color === marker.color) {
        isSelected = true;
      }
    });
    return isSelected;
  }

}
