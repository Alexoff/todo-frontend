import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoComponent } from './components/todo/todo.component';
import {TodoRoutingModule} from './todo-routing.module';
import { SelectComponent } from './components/select/select.component';
import { MarkersComponent } from './components/markers/markers.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ColorPickerModule} from 'ngx-color-picker';



@NgModule({
  declarations: [TodoComponent, SelectComponent, MarkersComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    ReactiveFormsModule,
    ColorPickerModule
  ]
})
export class TodoModule { }
